package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.TagRepository;
import com.example.beertag.services.impl.BeerServiceImpl;
import com.example.beertag.services.impl.TagServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl tagService;

    @InjectMocks
    BeerServiceImpl beerService;


    @Test
    public void getAllTagsTest(){

        Tag tag = new Tag();
        Tag tag1 = new Tag();
        Tag tag2 = new Tag();
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        tagList.add(tag1);
        tagList.add(tag2);
        when(tagRepository.getAll()).thenReturn(tagList);

        List<Tag> result = tagService.getAll();

        Assert.assertEquals(result,tagList);

    }

    @Test
    public void getByIdShould_ReturnTag_WhenTagExists(){

        Tag tag = new Tag();
        tag.setId(1);

        when(tagRepository.getById(1))
                .thenReturn(tag);

        Tag result = tagService.getById(1);

        Assert.assertEquals(result.getId(),tag.getId());
    }

    @Test
    public void updateTagTest(){
        Tag tag = new Tag();

        tagService.update(tag);

        verify(tagRepository,times(1)).update(tag);
        

    }

    @Test
    public void createTagTest(){
        Tag tag = new Tag();

        tagService.create(tag);

        verify(tagRepository,times(1)).create(tag);

    }

    @Test
    public void getTagByNameTest(){
            Tag tag = new Tag();
            tag.setName("testName");
            List<Tag> tagList = new ArrayList<>();
            tagList.add(tag);
        when(tagRepository.getByName("testName")).thenReturn(tagList);

        List<Tag> result = tagService.getByName("testName");

        Assert.assertEquals(result.get(0).getName(),tag.getName());


    }

    @Test
    public void getBeerByTagTest(){
        Beer beer = new Beer();
        beer.setName("beerName");
        Tag tag = new Tag();
        tag.setName("tagName");
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        beer.setTags(tagList);
        List<Beer> result = new ArrayList<>();
        result.add(beer);

        Assert.assertEquals(result.get(0).getName(),beer.getName());
    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowException_whenNameIsDuplicated(){
        Tag tag = new Tag();
        tag.setName("testName");
        when(tagRepository.checkTagExists(tag.getName())).thenReturn(true);
        tagService.create(tag);
    }

    @Test
    public void shouldReturnAllBeersWithSameTagTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("tagName");
        Beer beer1 = new Beer();
        beer1.setName("testName1");
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        beer.setTags(tagList);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);

        Mockito.when(tagRepository.getTagBeers(tag.getId())).thenReturn(beerList);

        List<Beer> result = tagService.getTagBeers(1);

        Assert.assertEquals(result,beerList);

    }

}
