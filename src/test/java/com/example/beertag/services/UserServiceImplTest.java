package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.*;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.repositories.RatingRepository;
import com.example.beertag.repositories.UserRepository;
import com.example.beertag.services.impl.BeerServiceImpl;
import com.example.beertag.services.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)

public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    BeerRepository beerRepository;

    @Mock
    RatingRepository ratingRepository;

    @InjectMocks
    UserServiceImpl userService;

    @InjectMocks
    BeerServiceImpl beerService;

    @Test
    public void getAllUsersTest(){

        User user = new User();
        User user1 = new User();
        User user2 = new User();
        List<User> userList = new ArrayList<>();
        userList.add(user);
        userList.add(user1);
        userList.add(user2);
        when(userRepository.getAll()).thenReturn(userList);

        List<User> result = userService.getAll();

        Assert.assertEquals(result,userList);

    }

    @Test
    public void getByIdShould_ReturnTag_WhenUserExists(){

        User user = new User();
        user.setId(1);

        when(userRepository.getById(1))
                .thenReturn(user);

        User result = userService.getById(1);

        Assert.assertEquals(result.getId(),user.getId());
    }

    @Test
    public void updateUserTest(){
        User user = new User();

        userService.update(user);

        verify(userRepository,times(1)).update(user);

    }

    @Test
    public void deleteUserTest(){
        User user = new User();

        userService.delete(user);

        verify(userRepository,times(1)).delete(user);

    }

    @Test
    public void createUserTest(){
        User user = new User();

        userService.create(user);

        verify(userRepository,times(1)).create(user);

    }

    @Test
    public void getUserByUsernameTest(){
        User user = new User();
        user.setUsername("testName");
        List<User> userList = new ArrayList<>();
        userList.add(user);
        when(userRepository.getByUsername("testName")).thenReturn(user);

        User result = userService.getByUsername(user.getUsername());

        Assert.assertEquals(result.getUsername(),user.getUsername());

    }
    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowException_whenNameIsDuplicated(){
        User user = new User();
        user.setEmail("testName");
        when(userRepository.checkUserExists(user.getEmail())).thenReturn(true);
        userService.create(user);
    }


    @Test
    public void addBeerToWishListTests(){
        Beer beer =  new Beer();
        beer.setId(1);
        User user = new User();
        user.setId(1);
        user.setUsername("username");
        Set<Beer> wishlist = new HashSet<>();
        wishlist.add(beer);
        user.setWishList(wishlist);
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        userService.addToWishList(user.getId(),beer.getId());
        verify(userRepository,times(1)).update(user);

    }

    @Test
    public void addBeerToDrankListTests(){
        Beer beer =  new Beer();
        beer.setId(1);
        User user = new User();
        user.setId(1);
        user.setUsername("username");
        Set<Beer> dranklist = new HashSet<>();
        dranklist.add(beer);
        user.setDrankBeer(dranklist);
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        userService.addToDrankList(user.getId(),beer.getId());
        verify(userRepository,times(1)).update(user);

    }

    @Test
    public void userPagingWhenSizeIsSMallerTest() {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setUsername("userTest");
        userList.add(user);
        userList.add(user);
        Mockito.when(userRepository.getAll()).thenReturn(userList);
        List<User> result = userService.userPaging(1, userList);

        Assert.assertEquals(userList.size(), result.size());
    }

    @Test
    public void userPagingWhenSizeIsBiggerTest() {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setUsername("userTest");
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        userList.add(user);
        Mockito.when(userRepository.getAll()).thenReturn(userList);
        List<User> result = userService.userPaging(1, userList);

        Assert.assertEquals(userList.size(), result.size());
    }

    @Test
    public void rateBeerShouldRateBeerWhenPassedCorrectInput(){
        User user = new User();
        user.setId(1);
        Beer beer = new Beer();
        beer.setId(1);
        Rating rating = new Rating();
        rating.setId(1);
        rating.setBeerId(1);
        rating.setUserId(1);
        rating.setRating(3.5);
        Set<Beer> beerList = new HashSet<>();
        beerList.add(beer);
        user.setDrankBeer(beerList);

        when(userRepository.getById(1)).thenReturn(user);
        when(beerRepository.getById(1)).thenReturn(beer);
        userService.addToDrankList(user.getId(),beer.getId());

        userService.rateBeer(user.getId(),beer.getId(),rating.getRating());

        verify(userRepository,times(1)).update(user);
    }

}
