package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Brewery;
import com.example.beertag.repositories.BreweryRepository;
import com.example.beertag.services.impl.BreweryServiceImpl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTest {

    @Mock
    BreweryRepository breweryRepository;

    @InjectMocks
    BreweryServiceImpl breweryService;


    @Test
    public void getAllBreweryTests(){

        Brewery brewery = new Brewery();
        Brewery brewery1 = new Brewery();
        Brewery brewery2 = new Brewery();
        List<Brewery> breweryList = new ArrayList<>();
        breweryList.add(brewery);
        breweryList.add(brewery1);
        breweryList.add(brewery2);

        when(breweryRepository.getAll()).thenReturn(breweryList);

        List<Brewery> result = breweryService.getAll();

        Assert.assertEquals(result,breweryList);

    }

    @Test
    public void getByIdShould_ReturnBrewery_WhenBreweryExists(){

        Brewery brewery = new Brewery();
        brewery.setId(1);

        when(breweryRepository.getById(1))
                .thenReturn(brewery);

        Brewery result = breweryService.getById(1);

        Assert.assertEquals(result.getId(),brewery.getId());
    }


    @Test
    public void updateBreweryTest(){
        Brewery brewery = new Brewery();

        breweryService.update(brewery);

        verify(breweryRepository,times(1)).update(brewery);


    }

    @Test(expected = DuplicateEntityException.class)
    public void createBreweryTest(){
        Brewery brewery = new Brewery();
        brewery.setName("testName");
        when(breweryRepository.checkBreweryExists(brewery.getName())).thenReturn(true);
        breweryService.create(brewery);
    }

    @Test
    public void shouldCreateBreweryRepositoryTest(){
        Brewery brewery = new Brewery();

        breweryService.create(brewery);

        verify(breweryRepository,times(1)).create(brewery);


    }
}
