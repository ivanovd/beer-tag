package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.CountryRepository;
import com.example.beertag.services.impl.CountryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    public void getById_Should_Return_Country_When_Exists() {

        Country country = new Country();
        country.setId(5);

        Mockito.when(countryRepository.getById(5)).thenReturn(country);

        Country returnedCountry = countryService.getById(5);

        Assert.assertEquals(returnedCountry.getId(),country.getId());

    }

    @Test
    public void getAll_Should_Return_All_Countries() {

        Country country1 = new Country();
        Country country2 = new Country();
        Country country3 = new Country();
        Country country4 = new Country();
        Country country5 = new Country();

        List<Country> countries = new ArrayList<>();

        countries.add(country1);
        countries.add(country2);
        countries.add(country3);
        countries.add(country4);
        countries.add(country5);

        Mockito.when(countryRepository.getAll()).thenReturn(countries);

        List<Country> result = countryService.getAll();

        Assert.assertEquals(result,countries);

    }

@Test
public void updateCountryTest(){
    Country country = new Country();

    countryService.update(country);

    verify(countryRepository,times(1)).update(country);

}

    @Test
    public void createCountryTest(){
        Country country = new Country();

        countryService.create(country);

        verify(countryRepository,times(1)).create(country);

    }

    @Test(expected = DuplicateEntityException.class)
    public void createCountryShouldThrowExceptionTests(){
        Country country = new Country();
        country.setName("testName");
        when(countryRepository.checkCountryExists(country.getName())).thenReturn(true);
        countryService.create(country);
    }

    @Test
    public void shouldReturnAllBeersFromOneCountry(){
        Beer beer = new Beer();
        beer.setName("testName");
        Country country = new Country();
        country.setId(1);
        country.setName("countryName");
        Beer beer1 = new Beer();
        beer1.setName("testName1");
        beer.setCountry(country);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);


        Mockito.when(countryRepository.getCountryBeers(country.getId())).thenReturn(beerList);

        List<Beer> result = countryService.getCountryBeers(1);

        Assert.assertEquals(result,beerList);

    }

}
