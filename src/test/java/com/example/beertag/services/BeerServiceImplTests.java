package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.*;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.repositories.TagRepository;
import com.example.beertag.services.impl.BeerServiceImpl;
import org.junit.Assert;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class   BeerServiceImplTests {

    @Mock
    BeerRepository beerRepository;

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    BeerServiceImpl beerService;

    @Test
    public void getByIdShould_ReturnBeer_WhenBeerExists(){

        Beer beer = new Beer();
        beer.setId(1);

        Mockito.when(beerRepository.getById(1))
                .thenReturn(beer);

        Beer returnedBeer = beerService.getById(1);

        Assert.assertEquals(returnedBeer.getId(),beer.getId());
    }

    @Test
    public void getAllBeersTest(){

        Beer beer = new Beer();
        Beer beer1 = new Beer();
        Beer beer2 = new Beer();
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer1);
        beerList.add(beer2);
        Mockito.when(beerRepository.getAll()).thenReturn(beerList);

        List<Beer> result = beerService.getAll();

        Assert.assertEquals(result,beerList);

    }

    @Test
    public void updateBeerTest(){
        Beer beer = new Beer();
        Country country = new Country ();
        country.setId(1);
        country.setName("tagName");
        Style style = new Style();
        style.setId(1);
        style.setName("nameStyle");
        Brewery brewery = new Brewery();
        brewery.setId(1);
        brewery.setName("breweryName");
        User user = new User();
        user.setId(1);
        user.setUsername("username");
        beer.setStyle(style);
        beer.setCountry(country);
        beer.setBrewery(brewery);
        beer.setUser(user);

        beerService.update(beer);

        verify(beerRepository,times(1)).update(beer);

    }

    @Test
    public void createBeerTest(){
        Beer beer = new Beer();

        beerService.create(beer);

        verify(beerRepository,times(1)).create(beer);

    }

    @Test
    public void getBeerByNameTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        List<Beer> tagList = new ArrayList<>();
        tagList.add(beer);
        when(beerRepository.getByName("testName")).thenReturn(tagList);

        List<Beer> result = beerService.getByName("testName");

        Assert.assertEquals(result.get(0).getName(),beer.getName());


    }

    @Test
    public void filterByCountryTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        Country country = new Country ();
        country.setId(1);
        country.setName("countryName");
        beer.setCountry(country);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        when(beerRepository.filterByCountry(country.getName())).thenReturn(beerList);

        List<Beer> result = beerService.filterByCountry(country.getName());

        Assert.assertEquals(result.get(0).getName(),beer.getName());
    }

    @Test
    public void filterByStyleTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        Style style = new Style();
        style.setId(1);
        style.setName("styleName");
        beer.setStyle(style);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        when(beerRepository.filterByStyle(style.getName())).thenReturn(beerList);

        List<Beer> result = beerService.filterByStyle(style.getName());

        Assert.assertEquals(result.get(0).getName(),beer.getName());
    }

    @Test
    public void filterByTagTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("tagName");
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        beer.setTags(tagList);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        when(beerRepository.filterByTags(tag.getName())).thenReturn(beerList);

        List<Beer> result = beerService.filterByTags(tag.getName());

        Assert.assertEquals(result.get(0).getName(),beer.getName());
    }

    @Test
    public void sortByName(){
        Beer beer = new Beer();
        beer.setName("B");
        Beer beer1 = new Beer();
        beer1.setName("A");
        List<Beer> beerList = beerService.sortByName();
        beerList.add(beer);
        beerList.add(beer1);
        when(beerRepository.sortByName()).thenReturn(beerList);

        List<Beer> result = beerService.sortByName();
        result.add(beer1);
        result.add(beer);

        Assert.assertEquals(result.toString(),beerList.toString());

    }

    @Test
    public void sortByABV(){
        Beer beer = new Beer();
        beer.setAbv(10);
        Beer beer1 = new Beer();
        beer1.setAbv(11);
        List<Beer> beerList = beerService.sortByAbv();
        beerList.add(beer);
        beerList.add(beer1);
        when(beerRepository.sortByAbv()).thenReturn(beerList);

        List<Beer> result = beerService.sortByAbv();
        result.add(beer1);
        result.add(beer);

        Assert.assertEquals(result.toString(),beerList.toString());

    }

    @Test
    public void sortByRating(){
        Beer beer = new Beer();
        beer.setRatings(2.5);
        Beer beer1 = new Beer();
        beer1.setRatings(2.0);
        List<Beer> beerList = beerService.sortByRating();
        beerList.add(beer);
        beerList.add(beer1);
        when(beerRepository.sortByRating()).thenReturn(beerList);

        List<Beer> result = beerService.sortByRating();
        result.add(beer1);
        result.add(beer);

        Assert.assertEquals(result.toString(),beerList.toString());

    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowException_whenNameIsDuplicated(){
        Beer beer = new Beer();
        beer.setName("testName");
        when(beerRepository.checkBeerExists(beer.getName())).thenReturn(true);
        beerService.create(beer);
    }

    @Test
    public void addTagToBeerTest(){
        Beer beer =  new Beer();
        beer.setId(1);
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("testTag");
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);
        beer.setTags(tagList);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        Mockito.when(beerRepository.getById(1)).thenReturn(beer);
        beerService.addBeerTag(beer.getId(),tag.getId());
        verify(beerRepository,times(1)).update(beer);

    }

    @Test
    public void beerPagingWhenSizeIsSMallerTest() {
        List<Beer> beerList = new ArrayList<>();
        Beer beer = new Beer();
        beer.setName("userTest");
        beerList.add(beer);
        beerList.add(beer);
        Mockito.when(beerRepository.getAll()).thenReturn(beerList);
        List<Beer> result = beerService.beerPaging(1, beerList);

        Assert.assertEquals(beerList.size(), result.size());
    }

    @Test
    public void beerPagingWhenSizeIsBiggerTest() {
        List<Beer> beerList = new ArrayList<>();
        Beer beer = new Beer();
        beer.setName("userTest");
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        beerList.add(beer);
        Mockito.when(beerRepository.getAll()).thenReturn(beerList);
        List<Beer> result = beerService.beerPaging(1, beerList);

        Assert.assertEquals(beerList.size(), result.size());
    }

}
