package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.*;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.repositories.StyleRepository;
import com.example.beertag.services.impl.StyleServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository styleRepository;

    @InjectMocks
    StyleServiceImpl styleService;


    @Test
    public void getAllStylesTests(){

        Style style = new Style();
        Style style1 = new Style();
        Style style2 = new Style();
        List<Style> styleList = new ArrayList<>();
        styleList.add(style);
        styleList.add(style1);
        styleList.add(style2);
        when(styleRepository.getAll()).thenReturn(styleList);

        List<Style> result = styleService.getAll();

        Assert.assertEquals(result,styleList);

    }

    @Test
    public void getByIdShould_ReturnStyle_WhenStyleExists(){

        Style style = new Style();
        style.setId(1);

        when(styleRepository.getById(1))
                .thenReturn(style);

        Style result = styleService.getById(1);

        Assert.assertEquals(result.getId(),style.getId());
    }

    @Test
    public void updateStyleTest(){
        Style style = new Style();

        styleService.update(style);

        verify(styleRepository,times(1)).update(style);


    }

    @Test
    public void createStyleTest(){
        Style style = new Style();

        styleService.create(style);

        verify(styleRepository,times(1)).create(style);

    }

    @Test
    public void shouldReturnAllBeersWithSameStyleTest(){
        Beer beer = new Beer();
        beer.setName("testName");
        Style style = new Style();
        style.setId(1);
        style.setName("styleName");
        Beer beer1 = new Beer();
        beer1.setName("testName1");
        beer.setStyle(style);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);

        Mockito.when(styleRepository.getStyleBeers(style.getId())).thenReturn(beerList);

        List<Beer> result = styleService.getStyleBeers(1);

        Assert.assertEquals(result,beerList);

    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowException_whenNameIsDuplicated(){
        Style style = new Style();
        style.setName("testName");
        when(styleRepository.checkStyleExists(style.getName())).thenReturn(true);
        styleService.create(style);
    }
}
