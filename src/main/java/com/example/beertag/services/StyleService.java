package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;

import java.util.List;

public interface StyleService {

     void create(Style style);

    void update(Style style);

     List<Style> getAll();

     Style getById(int id);

    List<Beer> getStyleBeers(int styleId);
}
