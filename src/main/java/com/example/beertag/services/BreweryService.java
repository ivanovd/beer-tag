package com.example.beertag.services;

import com.example.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {

    void create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    void update(Brewery brewery);

}
