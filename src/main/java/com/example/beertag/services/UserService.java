package com.example.beertag.services;

import com.example.beertag.models.Rating;
import com.example.beertag.models.User;

import java.util.List;

public interface UserService {

    void create(User user);

    List<User> getAll();

    User getById(int id);

    void update(User user);

    void delete(User user);

    User getByUsername(String username);

    void rateBeer(int userId, int beerId ,double rating);

    void addToWishList(int userId,int beerId);

    void addToDrankList(int userId, int beerID);

    List<Rating> sortTopThreeRating(int id);

    User getCurrentUser();

    List<User> userPaging(int currentPage, List<User> userList);

//    int totalPages(List<User> userList);
}
