package com.example.beertag.services;

import com.example.beertag.models.Beer;

import java.util.List;

public interface BeerService {

    void create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    void update(Beer beer);

    List<Beer> getByName(String name);

    List<Beer> filterByCountry(String name);

    List<Beer> filterByStyle(String name);

    List<Beer> filterByTags(String name);

    List<Beer> sortByName();

    List<Beer> sortByAbv();

    List<Beer> sortByRating();

    void addBeerTag(int beerId, int tagId);

    List<Beer> beerPaging(int currentPage, List<Beer> beerList);

//    int totalPages(List<Beer> beers);

}
