package com.example.beertag.services.impl;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.repositories.TagRepository;
import com.example.beertag.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.beertag.exceptions.DuplicateEntityException;

import java.util.ArrayList;
import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository repository;
    private TagRepository tagRepository;


    @Autowired
    public BeerServiceImpl(BeerRepository repository, TagRepository tagRepository) {
        this.repository = repository;
        this.tagRepository = tagRepository;
    }

    @Override
    public void create(Beer beer) {
        if (repository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format("Beer with name %s already exists", beer.getName())
            );
        }
        repository.create(beer);
    }

    @Override
    public List<Beer> getAll() {
        return repository.getAll();
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(Beer beer) {
        Beer originalBeer = repository.getById(beer.getId());

        if (beer.getCountry() == null) {
            beer.setCountry(originalBeer.getCountry());
        }
        if (beer.getBrewery() == null) {
            beer.setBrewery(originalBeer.getBrewery());
        }
        if (beer.getStyle() == null) {
            beer.setStyle(originalBeer.getStyle());
        }
        if (beer.getUser() == null) {
            beer.setUser(originalBeer.getUser());
        }

        repository.update(beer);
    }

    @Override
    public List<Beer> getByName(String name) {
       return repository.getByName(name);
    }

    @Override
    public List<Beer> filterByCountry(String name) {
        return repository.filterByCountry(name);
    }

    @Override
    public List<Beer> filterByStyle(String name) {
        return repository.filterByStyle(name);
    }

    @Override
    public List<Beer> filterByTags(String name) {
        return repository.filterByTags(name);
    }

    @Override
    public List<Beer> sortByName() {
        return repository.sortByName();
    }

    @Override
    public List<Beer> sortByAbv() {
        return repository.sortByAbv();
    }

    @Override
    public List<Beer> sortByRating() {
        return repository.sortByRating();
    }


    @Override
    public void addBeerTag(int beerId, int tagId) {
        Beer beer = repository.getById(beerId);
        Tag tag = tagRepository.getById(tagId);
        beer.getTags().add(tag);
        repository.update(beer);
    }

    @Override
    public List<Beer> beerPaging(int currentPage, List<Beer> beerList){
        List<Beer> showBeers = new ArrayList<>();
        int currentElement = ((currentPage - 1) * 10 ) + 1;
        int lastElement;
        if (getAll().size() < currentElement + 9)
        {
            lastElement = getAll().size() - currentElement;
            lastElement += currentElement;
        }
        else {
            lastElement = currentElement + 9;
        }
        for (int i = currentElement-1; i <= lastElement - 1 ; i++) {
            showBeers.add(beerList.get(i));
        }
        return showBeers;
    }

//    @Override
//    public int totalPages(List<Beer> beers){
//        if(beers.size() % 10 == 0){
//            return beers.size() / 10;
//        }
//        else {
//            return (beers.size() / 10) + 1;
//        }
//    }

}
