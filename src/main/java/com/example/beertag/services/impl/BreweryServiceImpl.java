package com.example.beertag.services.impl;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Brewery;
import com.example.beertag.repositories.BreweryRepository;
import com.example.beertag.services.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository repository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Brewery brewery) {
        if(repository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(String.format("Brewery with name %s already exists", brewery.getName()));
        }
        repository.create(brewery);
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(Brewery brewery) {
        repository.update(brewery);
    }

}
