package com.example.beertag.services.impl;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.repositories.StyleRepository;
import com.example.beertag.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository repository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Style style) {
        if(repository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(String.format("Style with name %s already exists", style.getName()));
        }
        repository.create(style);
    }

    @Override
    public void update(Style style) {
        repository.update(style);
    }

    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }

    @Override
    public Style getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Beer> getStyleBeers(int styleId) {
        return repository.getStyleBeers(styleId);
    }

}

