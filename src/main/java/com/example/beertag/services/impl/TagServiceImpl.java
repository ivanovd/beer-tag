package com.example.beertag.services.impl;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.TagRepository;
import com.example.beertag.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void create(Tag tag) {
        if(tagRepository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(String.format("Tag with name %s already exists", tag.getName()));
        }
        tagRepository.create(tag);
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.getById(id);
    }

    @Override
    public void update(Tag tag) {
        tagRepository.update(tag);
    }

    @Override
    public List<Beer> getTagBeers(int tagId) {
        return tagRepository.getTagBeers(tagId);
    }

    @Override
    public List<Tag> getByName(String name) {
        return tagRepository.getByName(name);
    }
}
