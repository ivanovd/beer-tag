package com.example.beertag.services.impl;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import com.example.beertag.repositories.CountryRepository;
import com.example.beertag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Country country) {
        if (repository.checkCountryExists(country.getName())) {
            throw new DuplicateEntityException(String.format("Country with name %s already exists", country.getName()));
        }
        repository.create(country);
    }

    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public Country getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(Country country) {
        repository.update(country);
    }

    @Override
    public List<Beer> getCountryBeers(int countryId) {
        return repository.getCountryBeers(countryId);
    }
}
