package com.example.beertag.services.impl;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.repositories.RatingRepository;
import com.example.beertag.repositories.UserRepository;
import com.example.beertag.services.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private BeerRepository beerRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BeerRepository beerRepository,
                           RatingRepository ratingRepository) {
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void create(User user) {
        if(userRepository.checkUserExists(user.getEmail())){
            throw new DuplicateEntityException(String.format("User with email %s already exists!",user.getEmail()));
        }
        userRepository.create(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public User getByUsername(String username) {
      return userRepository.getByUsername(username);
    }

    @Override
    public void rateBeer(int userId, int beerId, double rating) {
        User user = userRepository.getById(userId);
        Beer beer = beerRepository.getById(beerId);
        Rating rating1 = new Rating();
        rating1.setUserId(user.getId());
        rating1.setBeerId(beer.getId());
        if (rating < 1 || rating > 5){
            throw new IllegalArgumentException("Rating must be between 1 and 5 ");
        }
        else {
            rating1.setRating(rating);
        }

        if (user.getDrankBeer().stream().anyMatch(beer1 -> beer1.getId() == beerId)){
            ratingRepository.create(rating1);
            double ratingToGet = ratingRepository.averageRating(beer.getId());
            beer.setRatings(ratingToGet);
            beerRepository.update(beer);
        }
        else{
            throw new EntityNotFoundException(
                    "You can't rate beer if it doesn't exists in your drank list!");
        }
    }

    @Override
    public void addToWishList(int userId,int beerId) {
        User user = userRepository.getById(userId);
        Beer beer1 = beerRepository.getById(beerId);
        user.getWishList().add(beer1);
        userRepository.update(user);
    }

    @Override
    public void addToDrankList(int userId, int beerID) {
        User user = userRepository.getById(userId);
        Beer beer = beerRepository.getById(beerID);
        user.getDrankBeer().add(beer);
        userRepository.update(user);
    }

    @Override
    public List<Rating> sortTopThreeRating(int id) {
        User user = userRepository.getById(id);
        return ratingRepository.getRatingByUser(user.getId());
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        return getByUsername(username);
    }

    @Override
    public List<User> userPaging(int currentPage, List<User> userList){
        List<User> showUsers = new ArrayList<>();
        int currentElement = ((currentPage - 1) * 10 ) + 1;
        int lastElement;
        if (getAll().size() < currentElement + 9)
        {
            lastElement = getAll().size() - currentElement;
            lastElement += currentElement;
        }
        else {
            lastElement = currentElement + 9;
        }
        for (int i = currentElement-1; i <= lastElement - 1 ; i++) {
            showUsers.add(userList.get(i));
        }
        return showUsers;
    }

//    @Override
//    public int totalPages(List<User> users){
//        if(users.size() % 10 == 0){
//            return users.size() / 10;
//        }
//        else {
//            return (users.size() / 10) + 1;
//        }
//    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
}
