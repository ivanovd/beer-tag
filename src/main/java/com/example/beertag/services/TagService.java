package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;

import java.util.List;

public interface TagService {

    void create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    List<Tag> getByName(String name);

    void update(Tag tag);

    List<Beer> getTagBeers(int tagId);
}
