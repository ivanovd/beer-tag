package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;

import java.util.List;

public interface CountryService {

    void create(Country country);

    List<Country> getAll();

    Country getById(int id);

    void update(Country country);

    List<Beer> getCountryBeers(int countryId);

}
