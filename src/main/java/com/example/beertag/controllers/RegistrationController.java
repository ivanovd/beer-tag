package com.example.beertag.controllers;

import com.example.beertag.models.Authorities;
import com.example.beertag.models.User;
import com.example.beertag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.*;

import java.util.Date;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder,
                                  UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new com.example.beertag.models.User());
        return "register";
    }

    @PostMapping(value = "/register")
    public String registerUser(@Valid @ModelAttribute("user")com.example.beertag.models.User user,
                               BindingResult bindingResult,
                               Model model,
                               @RequestParam("file") MultipartFile multipart) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);

        com.example.beertag.models.User fatUser = userService.getByUsername(user.getUsername());
        fatUser.setUsername(user.getUsername());
        fatUser.setPassword(passwordEncoder.encode(user.getPassword()));
        fatUser.setEmail(user.getEmail());
        fatUser.setFirstName(user.getFirstName());
        fatUser.setLastName(user.getLastName());
        String nameImage = uploadFile(multipart);
        fatUser.setProfilePicture(nameImage);
        userService.update(fatUser);

        return "login";
    }

    @PostMapping("/authenticate")
    public String showRegisterConfirmation() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Authorities authorities = new Authorities();
        authorities.getAuthority().equals(authentication);
        return "register";
    }

    @GetMapping("users/edit")
    public String editUser(Model model) {
        User user = userService.getCurrentUser();
        model.addAttribute("user", user);
        return "edit-user";
    }

    @PostMapping("/users/{username}/update")
    public String updateUser(Model model,
                             @PathVariable String username,
                             @ModelAttribute("user") User newUser,
                             @RequestParam("file") MultipartFile multipart) {
        User user = userService.getByUsername(username);
        user.setUsername(username);
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        if(!multipart.isEmpty()){
            String nameImage = uploadFile(multipart);
            user.setProfilePicture(nameImage);
        }
        userService.update(user);

        return "users-view";
    }

    private String uploadFile(MultipartFile file) {
         if(!file.isEmpty()){
             try {
                 byte[] bytes = file.getBytes();
                 String rootPath = "C:\\uploads";
                 File dir = new File("C:/");
                 if(!dir.exists()){
                     dir.mkdirs();
                 }
                 String name = String.valueOf("/uploads/"+new Date().getTime()) + ".jpg";
                 File serverFile = new File(dir.getAbsolutePath()
                         +File.separator + name);
                 BufferedOutputStream stream = new BufferedOutputStream(
                         new FileOutputStream(serverFile));
                 stream.write(bytes);

                 return name;
             }
             catch (IOException e){
                 e.printStackTrace();
             }
         } else {
             return "/uploads/user.jpg";
         }
         return null;
    }
}
