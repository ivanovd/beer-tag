package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.models.dto.BeerDTO;
import com.example.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private BeerService service;
    private CountryService countryService;
    private BreweryService breweryService;
    private StyleService styleService;
    private UserService userService;

    @Autowired
    public BeerRestController(BeerService service, CountryService countryService, BreweryService breweryService,
                              StyleService styleService, UserService userService) {
        this.service = service;
        this.countryService = countryService;
        this.breweryService = breweryService;
        this.styleService = styleService;
        this.userService = userService;
    }

    @GetMapping
    public List<Beer> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDTO newBeer) {
        try {
            Beer beer = new Beer();
            beer.setName(newBeer.getName());
            beer.setAbv(newBeer.getAbv());
            beer.setDescription(newBeer.getDescription());

            Country country = countryService.getById(newBeer.getCountryId());
            beer.setCountry(country);

            Brewery brewery = breweryService.getById(newBeer.getBreweryId());
            beer.setBrewery(brewery);

            Style style = styleService.getById(newBeer.getStyleId());
            beer.setStyle(style);

            User user = userService.getById(newBeer.getUserId());
            beer.setUser(user);

            service.create(beer);
            return beer;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid BeerDTO newBeer) {
        try {
            Beer beer = service.getById(id);

            beer.setName(newBeer.getName());
            beer.setAbv(newBeer.getAbv());


            if (newBeer.getDescription() == null) {
                beer.setDescription(newBeer.getDescription());
            }

            if (newBeer.getCountryId() > 0) {
                Country country = countryService.getById(newBeer.getCountryId());
                beer.setCountry(country);
            }

            if (newBeer.getBreweryId() > 0) {
                Brewery brewery = breweryService.getById(newBeer.getBreweryId());
                beer.setBrewery(brewery);
            }

            if (newBeer.getStyleId() > 0) {
                Style style = styleService.getById(newBeer.getStyleId());
                beer.setStyle(style);
            }

            if (newBeer.getUserId() > 0) {
                User user = userService.getById(newBeer.getUserId());
                beer.setUser(user);
            }

            service.update(beer);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/delete/{id}")
    public void delete(@PathVariable int id) {
        try {
            Beer beer = service.getById(id);

            beer.setDeleted(true);

            service.update(beer);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("beer-tag/{beerId}/{tagId}")
    public void addBeerTag(@PathVariable int beerId, @PathVariable int tagId) {
        service.addBeerTag(beerId, tagId);
    }

    @PostMapping("beer-tag/{beerId}")
    public List<Tag> getAllTags(@PathVariable int beerId) {
        Beer beer = service.getById(beerId);
        return beer.getTags();
    }
}