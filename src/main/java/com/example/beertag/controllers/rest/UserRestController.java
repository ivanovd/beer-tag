package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private BeerService beerService;
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService, BeerService beerService) {
        this.userService = userService;
        this.beerService = beerService;
    }

    @GetMapping
    public List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id){
        try {
            return userService.getById(id);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public User createUser(@RequestBody @Valid User newUser){
        try {
            //todo sloji dto
            //todo sloji snimka
            User user = new User();
//            Image image = new Image();
//            image.setUrl("C:\\Users\\Petyo\\Desktop\\Beers images");
            user.setUsername(newUser.getUsername());
            user.setEmail(newUser.getEmail());
            user.setFirstName(newUser.getFirstName());
            user.setLastName(newUser.getLastName());
            user.setPassword(newUser.getPassword());
//            user.setProfilePicture(image.getUrl());
            userService.create(newUser);

            return newUser;
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public User editUser(@PathVariable int id, @RequestBody @Valid User newUser){
        try {
            User user= userService.getById(id);
            user.setUsername(newUser.getUsername());
            user.setEmail(newUser.getEmail());
            user.setFirstName(newUser.getFirstName());
            user.setLastName(newUser.getLastName());
            user.setPassword(newUser.getPassword());
            userService.update(user);
            return user;
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/delete/{id}")
    public void deleteUser(@PathVariable int id) {
        try {
            User user = userService.getById(id);

            user.setEnabled(false);

            userService.update(user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("username/{username}")
    public User getByUsername(@PathVariable String username){
        try {
            return userService.getByUsername(username);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @GetMapping("/wishlist/{id}")
    public Set<Beer> getAllWishListBeers(@PathVariable int id){
        User user = userService.getById(id);
        return user.getWishList();
    }

    @GetMapping("/dranklist/{id}")
    public Set<Beer> getAllDrankListBeers(@PathVariable int id){
        User user = userService.getById(id);
        return user.getDrankBeer();
    }

    @PostMapping("add-to-wish-list/{beerID}/{userId}")
    public void addToWishList(@PathVariable int beerID, @PathVariable int userId){
        userService.addToWishList(userId,beerID);
    }

    @PostMapping("add-to-drank-list/{beerID}/{userId}")
    public void addToDrankList(@PathVariable int beerID, @PathVariable int userId){
        userService.addToDrankList(userId,beerID);
    }

    @PostMapping("add-rating-to-beer/{beerID}/{userId}/{rating}")
    public void addRatingToBeer(@PathVariable int beerID, @PathVariable int userId, @PathVariable double rating){
        userService.rateBeer(userId,beerID,rating);
    }
}
