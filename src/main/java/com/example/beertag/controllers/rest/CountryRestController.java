package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import com.example.beertag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {
    private CountryService countryService;

    @Autowired
    public CountryRestController(CountryService styleService) {
        this.countryService = styleService;
    }

    @GetMapping
    public List<Country> getAll(){
        return countryService.getAll();
    }

    @GetMapping("/{id}")
    public Country getCountryById(@PathVariable int id){
        try {
            return countryService.getById(id);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create-country")
    public Country createCountry(@RequestBody @Valid Country newCountry){
        try {
            Country country = new Country();
            country.setName(newCountry.getName());
            countryService.create(newCountry);
            return newCountry;
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Country editCountry(@PathVariable int id, @RequestBody @Valid Country newCountry){
        try {
            Country country = countryService.getById(id);
            country.setName(newCountry.getName());
            countryService.update(country);
            return country;
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/delete/{id}")
    public void delete(@PathVariable int id) {
        try {
            Country country = countryService.getById(id);

            country.setDeleted(true);

            countryService.update(country);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/get-beers/{countryId}")
    public List<Beer> getCountryBeers(@PathVariable int countryId){
        try {
            return countryService.getCountryBeers(countryId);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
