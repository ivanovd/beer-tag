package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.services.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
    public class BreweryRestController {
        private BreweryService breweryService;

        @Autowired
        public BreweryRestController(BreweryService styleService) {
            this.breweryService = styleService;
        }

        @GetMapping
        public List<Brewery> getAll(){
            return breweryService.getAll();
        }

        @GetMapping("/{id}")
        public Brewery getBreweryById(@PathVariable int id){
            try {
                return breweryService.getById(id);
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

        @PostMapping("/create-brewery")
        public Brewery createBrewery(@RequestBody @Valid Brewery newBrewery){
            try {
                Brewery brewery = new Brewery();
                brewery.setName(newBrewery.getName());
                breweryService.create(newBrewery);
                return newBrewery;
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
            }
        }

        @PutMapping("/{id}")
        public Brewery editBrewery(@PathVariable int id , @RequestBody @Valid Brewery newBrewery){
            try {
                Brewery brewery = breweryService.getById(id);
                brewery.setName(newBrewery.getName());
                breweryService.update(brewery);
                return brewery;
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

    @PutMapping("/delete/{id}")
    public void delete(@PathVariable int id) {
        try {
            Brewery brewery = breweryService.getById(id);

            brewery.setDeleted(true);

            breweryService.update(brewery);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
