package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

        private StyleService styleService;

        @Autowired
        public StyleRestController(StyleService styleService) {
            this.styleService = styleService;
        }

        @GetMapping
        public List<Style> getAll(){
            return styleService.getAll();
        }

         @GetMapping("/{id}")
        public Style getStyleById(@PathVariable int id){
            try {
                return styleService.getById(id);
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

        @PostMapping("/create-style")
        public Style createStyle(@RequestBody @Valid Style newStyle){
            try {
                Style style = new Style();
                style.setName(newStyle.getName());
                styleService.create(newStyle);
                return newStyle;
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
            }
        }

        @PutMapping("/{id}")
        public Style editStyle(@PathVariable int id, @RequestBody @Valid Style newStyle){
            try {
                Style style = styleService.getById(id);
                style.setName(newStyle.getName());
                styleService.update(style);
                return style;
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

    @PutMapping("/delete/{id}")
    public void deleteStyle(@PathVariable int id) {
        try {
            Style style = styleService.getById(id);

            style.setDeleted(true);

            styleService.update(style);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

        @GetMapping("/{styleId}/beers")
        public List<Beer> getStyleBeers(@PathVariable int styleId){
            try {
                return styleService.getStyleBeers(styleId);
            }
            catch (DuplicateEntityException e){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }
}
