package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    private TagService tagService;

    @Autowired
    public TagRestController(TagService styleService) {
        this.tagService = styleService;
    }

    @GetMapping
    public List<Tag> getAll(){
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable int id){
        try {
            return tagService.getById(id);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@RequestBody @Valid Tag newTag){
        try {
            Tag tag = new Tag();
            tag.setName(newTag.getName());
            tagService.create(newTag);
            return newTag;
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag editTag(@PathVariable int id, @RequestBody @Valid Tag newTag){
        try {
            Tag tag = tagService.getById(id);
            tag.setName(newTag.getName());
            tagService.update(tag);
            return tag;
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/delete/{id}")
    public void deleteTag(@PathVariable int id) {
        try {
            Tag tag = tagService.getById(id);

            tag.setDeleted(true);

            tagService.update(tag);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{tagId}/tag")
    public List<Beer> getTagBeers(@PathVariable int tagId){
        try {
            return tagService.getTagBeers(tagId);
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}