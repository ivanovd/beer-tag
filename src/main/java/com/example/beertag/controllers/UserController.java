package com.example.beertag.controllers;

import com.example.beertag.models.User;
import com.example.beertag.models.dto.UserDTO;
import com.example.beertag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {
    private UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service; }

    @GetMapping("/admin/users")
    public String showUsers(Model model) {
        model.addAttribute("users", service.userPaging(1, service.getAll()));
        return "users";
    }

    @GetMapping("/admin/users/{page}")
    public String showUsers(Model model, @PathVariable int page) {
        model.addAttribute("users", service.userPaging(page, service.getAll()));
        return "users";
    }


    @GetMapping("/admin/users/new")
    public String showNewUserForm(Model model) {
        model.addAttribute("user", new UserDTO());
        return "users";
    }

    @PostMapping("/admin/users/new")
    public String createUser(@Valid @ModelAttribute("user") User user, BindingResult errors) {
        if (errors.hasErrors()) {
            return "users";
        }
        User toCreate = new User();
        toCreate.setUsername(user.getUsername());
        toCreate.setLastName(user.getLastName());
        toCreate.setEnabled(user.isEnabled());
        service.create(toCreate);
        return "redirect:/users";
    }

    @GetMapping("/users")
    public String usersView(){
        return "users-view";
    }

        @GetMapping("/user/details")
        public String showInformation(Model model, Principal principal){
            User user = service.getByUsername(principal.getName());

            model.addAttribute("show-information", user.getUsername());
            model.addAttribute("users",user);
            return "show-information";
        }

    @PostMapping("admin/users/disable/{userName}")
    public String disableUser(@PathVariable String userName){
        service.delete(service.getByUsername(userName));
        return "users";
    }
}