package com.example.beertag.controllers;

import com.example.beertag.services.BeerService;
import com.example.beertag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {

    private BeerService beerService;
    private CountryService countryService;

    @Autowired
    public HomeController(BeerService beerService,CountryService countryService) {
        this.beerService = beerService;
        this.countryService = countryService;
    }

    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }


}
