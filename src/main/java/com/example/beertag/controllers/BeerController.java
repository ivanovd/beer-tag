package com.example.beertag.controllers;

import com.example.beertag.models.*;
import com.example.beertag.models.dto.BeerDTO;
import com.example.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Controller
public class BeerController {

    private BeerService beerService;
    private StyleService styleService;
    private CountryService countryService;
    private BreweryService breweryService;
    private UserService userService;
    private TagService tagService;

    @Autowired
    public BeerController(BeerService beerService, StyleService styleService,
                          CountryService countryService, BreweryService breweryService,
                          UserService userService, TagService tagService) {
        this.beerService = beerService;
        this.styleService = styleService;
        this.countryService = countryService;
        this.breweryService = breweryService;
        this.userService = userService;
        this.tagService = tagService;
    }

    @GetMapping("/beers")
    public String showBeers(Model model) {
        model.addAttribute("beers", beerService.beerPaging(1, beerService.getAll()));
        return "beers";
    }

    @GetMapping("/beers/{page}")
    public String showBeers(Model model, @PathVariable int page) {
        model.addAttribute("beers", beerService.beerPaging(page, beerService.getAll()));
        return "beers";
    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return styleService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTag() {
        return tagService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBreweries() {
        return breweryService.getAll();
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("beer", new BeerDTO());
        return "beer";
    }

    @PostMapping(value ="/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") BeerDTO beer,Model model,
                             BindingResult bindingResult,
                             @RequestParam("image")MultipartFile multipartFile) {

        Beer toCreate = new Beer();
        toCreate.setName(beer.getName());
        toCreate.setAbv(beer.getAbv());
        toCreate.setStyle(styleService.getById(beer.getStyleId()));
        toCreate.setCountry(countryService.getById(beer.getCountryId()));
        toCreate.setBrewery(breweryService.getById(beer.getBreweryId()));
        toCreate.setDescription(beer.getDescription());
        toCreate.setUser(userService.getCurrentUser());
        String image = uploadFile(multipartFile);
        toCreate.setPicture(image);
        beerService.create(toCreate);
        return "redirect:/beers";
    }

    private String uploadFile(MultipartFile file) {
        if(!file.isEmpty()){
            try {
                byte[] bytes = file.getBytes();
                String rootPath = "C:\\uploads";
                File dir = new File("C:/");
                if(!dir.exists()){
                    dir.mkdirs();
                }
                String name = String.valueOf("/uploads/"+new Date().getTime()) + ".jpg";
                File serverFile = new File(dir.getAbsolutePath()
                        +File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);

                return name;
            }
            catch (IOException e){
                e.printStackTrace();
            }
        } else {
            return "/uploads/beer.jpg";
        }
        return null;
    }


    @GetMapping("/wishlist")
    public String getWishList(Model model) {
        User user = userService.getCurrentUser();
        Set<Beer> beerList = userService.getByUsername(user.getUsername()).getWishList();
        model.addAttribute("beers", beerList);
        return "wishlist";
    }

    @GetMapping("/dranklist")
    public String getDrankList(Model model) {
        User user = userService.getCurrentUser();
        Set<Beer> beerList = userService.getByUsername(user.getUsername()).getDrankBeer();
        model.addAttribute("beers", beerList);
        return "dranklist";
    }

    @PostMapping("beers/wishlist-add/{beerName}")
    public String AddBeerToWishlist(@PathVariable String beerName, Principal principal){
        Beer beer = beerService.getByName(beerName).get(0);
        User user = userService.getByUsername(principal.getName());
        userService.addToWishList(user.getId(), beer.getId());
        return "beers";
    }

    @PostMapping("beers/dranklist-add/{beerName}")
    public String AddBeerToDranklist(@PathVariable String beerName, Principal principal){
        Beer beer = beerService.getByName(beerName).get(0);
        User user = userService.getByUsername(principal.getName());
        userService.addToDrankList(user.getId(), beer.getId());
        return "beers";
    }


    @PostMapping("beers/rate-beer/{beerName}")
    public String rateBeer(@PathVariable String beerName, Principal principal,
                           @RequestParam(defaultValue = "")String rating,
                           Model model) {
        Beer beer1 = beerService.getByName(beerName).get(0);
        User user = userService.getByUsername(principal.getName());

        int rate = Integer.parseInt(rating);
        beer1.setRatings(rate);
        userService.rateBeer(user.getId(), beer1.getId(), rate);
        model.addAttribute("rating", beerService.getAll());
        return "dranklist";
    }

    @GetMapping("/topRating")
    public String topRatingBeers(Principal principal,Model model){
        User user = userService.getByUsername(principal.getName());
        List<Rating> rating = userService.sortTopThreeRating(user.getId());
        Beer beer = beerService.getById (rating.get(0).getBeerId());
        Beer beer1 = beerService.getById (rating.get(1).getBeerId());
        Beer beer2 = beerService.getById (rating.get(2).getBeerId());
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer1);
        beerList.add(beer2);
        model.addAttribute("beers",beerList);
        return "top-three";
    }

    @GetMapping("/beers/filterByCountry")
    public String filterByCountry(Model model,
                                  @RequestParam( defaultValue = "") String name){
        List<Beer> beerList = beerService.filterByCountry(name);
        model.addAttribute("beers",beerList);
        return "beers";
    }

    @GetMapping("beers/filterByTag")
    public String filterByTag(Model model,
                              @RequestParam(defaultValue = "") String name){
        List<Beer> beerList = beerService.filterByTags(name);
        model.addAttribute("beers",beerList);
        return "beers";
    }

    @GetMapping("/beers/filterByStyle")
    public String filterByStyle(Model model,
                                @RequestParam(defaultValue = "")String name){
        List<Beer> beerList = beerService.filterByStyle(name);
        model.addAttribute("beers",beerList);
        return "beers";
    }

    @GetMapping("/beers/sortByName")
    public String sortByName(Model model){
        beerService.getAll();
        model.addAttribute("beers",beerService.sortByName());
        return "beers";
    }

    @GetMapping("/beers/sortByRating")
    public String SortByRating(Model model){
        model.addAttribute("beers",beerService.sortByRating());
        return "beers";
    }

    @GetMapping("/beers/sortByAbv")
    public String sortByAbv(Model model){
        model.addAttribute("beers",beerService.sortByAbv());
        return "beers";
    }
}
