package com.example.beertag.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private DataSource securityDataSource;

    @Autowired
    public SecurityConfig(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }



    @Override
    public void configure(WebSecurity webSecurity){
        webSecurity.ignoring().antMatchers("/uploads/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .csrf().disable()
            .httpBasic()
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/api/**").hasAnyRole("USER","ADMIN")
            .antMatchers(HttpMethod.PUT, "/api/**").hasAnyRole("USER","ADMIN")
            .antMatchers("/api/users/**").hasAnyRole("USER","ADMIN")
            .antMatchers("/api/beers/").permitAll()
            .antMatchers("/uploads/**").permitAll()
            .antMatchers("/beers").permitAll()
            .antMatchers("/beers/**").permitAll()
            .antMatchers("/beers/new").hasAnyRole("USER","ADMIN")
//            .antMatchers("/beers/**").permitAll()
            .antMatchers("/admin/**").hasRole("ADMIN")
            .antMatchers("/").permitAll()
            .antMatchers("/register").permitAll()
            .antMatchers("/**").permitAll()
            .and()
            .formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/authenticate")
            .failureUrl("/access-denied").permitAll()
            .and()
            .logout().permitAll()
            .logoutSuccessUrl("/").permitAll()
            .and()
            .exceptionHandling()
            .accessDeniedPage("/access-denied");
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
