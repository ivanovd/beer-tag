package com.example.beertag.repositories;

import com.example.beertag.models.*;

import java.util.List;

public interface UserRepository {
    void create(User user);

    void update(User user);

    User getById(int id);

    List<User> getAll();

    boolean checkUserExists(String email);

    User getByEmail(String name);

    User getByUsername(String username);

    void delete(User user);

}
