package com.example.beertag.repositories;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import org.hibernate.Session;

import java.util.List;

public interface CountryRepository {

    void create(Country country);

    List<Country> getAll();

    Country getById(int id);

    Country getById(int id, Session session);

    boolean checkCountryExists(String name);

    void update(Country country);

    List<Beer> getCountryBeers(int countryId);

    List<Country> getByName(String name);

}
