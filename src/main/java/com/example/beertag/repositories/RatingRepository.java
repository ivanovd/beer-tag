package com.example.beertag.repositories;

import com.example.beertag.models.Rating;

import java.util.List;

public interface RatingRepository {

    double averageRating(int beerId);

    void addRating(double rating);

    void create(Rating rating);

    List<Rating> getRatingByUser(int userId);
}
