package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import com.example.beertag.repositories.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private SessionFactory sessionFactory;

    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
        }
    }

    @Override
    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(
                    "from Country where isDeleted = false", Country.class).list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return getById(id, session);
        }
    }

    @Override
    public Country getById(int id, Session session) {
        Country country = session.get(Country.class, id);
        if (country == null || country.isDeleted()) {
            throw new EntityNotFoundException(
                    String.format("Country with id %d not found", id));
        }
        return country;
    }

    @Override
    public boolean checkCountryExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public void update(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Beer> getCountryBeers(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            Country country = getById(countryId, session);
            return new ArrayList<>(country.getBeers());
        }
    }

    @Override
    public List<Country> getByName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Country> query = session.createQuery(
                    "from Country where name LIKE :name and isDeleted = false", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

}
