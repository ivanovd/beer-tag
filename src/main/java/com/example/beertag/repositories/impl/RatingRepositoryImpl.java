package com.example.beertag.repositories.impl;

import com.example.beertag.models.Rating;
import com.example.beertag.repositories.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public double averageRating(int beerId) {
        try (Session session = sessionFactory.openSession()){
            Query<Rating> query = session.createQuery(
                    "from Rating where beerId = :beerId");
            query.setParameter("beerId",beerId);;
            List<Rating> ratingList = query.list();
            double sum = 0;
            for (Rating rating: ratingList) {
                sum += rating.getRating();
            }
           double finalSum = sum / ratingList.size();
            return finalSum;
        }
    }

    @Override
    public void addRating(double rating) {
        try (Session session = sessionFactory.openSession()){
            session.save(rating);
        }
    }

    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()){
            session.save(rating);
        }
    }

    @Override
    public List<Rating> getRatingByUser(int userId) {
        try (Session session = sessionFactory.openSession()){
            Query<Rating> query = session.createQuery(
                    "from Rating where userId = :userId");
            query.setParameter("userId", userId);
            List<Rating> ratingSort = query.list();
            ratingSort.sort(Comparator.comparing(Rating::getRating));//metod referenciq
            List<Rating> topThree = new ArrayList<>();
            int i =0;
            int size = ratingSort.size()-1;
           while (i < 3 || ratingSort.isEmpty()){
               topThree.add(ratingSort.get(size));
               size--;
               i++;
           }
            return topThree;
        }
    }
}
