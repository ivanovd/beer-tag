package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.repositories.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
   private SessionFactory sessionFactory;

   @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()){
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    public void delete(User user) {
        User newUser = getById(user.getId());

        newUser.setEnabled(false);

        update(newUser);
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()){
            User user = session.get(User.class,id);
            if(user == null || !user.isEnabled()){
                throw new EntityNotFoundException(String.format("User with id %s does not exist", id));
            }
            return user;
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(
                    "from User where enabled = true",User.class).list();
        }
    }

    @Override
    public boolean checkUserExists(String email) {
        return getByEmail(email) != null;
    }

    @Override
    public User getByEmail(String email) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where email = :email and enabled = true");
            query.setParameter("email", email);
            List<User> users = query.list();
            if(users.size() != 1){
                throw new EntityNotFoundException(
                        String.format("User with email %s does not exist", email)
                );
            }
           return users.get(0);
        }
    }

    @Override
    public User getByUsername(String username) {
         try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where username = :username and enabled = true");
            query.setParameter("username", username);
            List<User> users = query.list();
            if(users.size() != 1){
                throw new EntityNotFoundException(
                        String.format("User with username %s does not exist", username)
                );
            }
            return users.get(0);
         }
    }

}
