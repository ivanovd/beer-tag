package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.repositories.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Style style) {
            try (Session session = sessionFactory.openSession()) {
                session.save(style);
            }
    }

    @Override
    public void update(Style style) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkStyleExists(String name) {
        return getByName(name).size()!=0;
    }

    @Override
    public List<Style> getAll() {
        try(Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(
                            "from Style where isDeleted = false", Style.class)
                    .list();
        }
    }

    @Override
    public List<Style> getByName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Style> query = session.createQuery(
                    "from Style where name LIKE :name and isDeleted = false", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try(Session session = sessionFactory.openSession()){
           return getById(id,session);
        }
    }

    @Override
    public List<Beer> getStyleBeers(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            Style style = getById(styleId,session);
            return new ArrayList<>(style.getBeers());
        }
    }

    private Style getById(int id, Session session) {
        Style style = session.get(Style.class, id);
        if (style == null || style.isDeleted()) {
            throw new EntityNotFoundException(
                    String.format("Style with id %d does not exist.", id)
            );
        }
        return style;
    }
}
