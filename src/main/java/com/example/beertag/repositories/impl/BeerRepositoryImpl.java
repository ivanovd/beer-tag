package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.BeerRepository;
import com.example.beertag.services.TagService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;
    private TagService tagService;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory, TagService tagService) {
        this.sessionFactory = sessionFactory;
        this.tagService = tagService;
    }

    @Override
    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(
                    "from Beer where isDeleted = false",Beer.class)
                    .list();


        }
    }

    @Override
    public boolean checkBeerExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null || beer.isDeleted()) {
                throw new EntityNotFoundException(
                        String.format("Beer with id %d not found", id));
            }
            return beer;
        }
    }

    @Override
    public List<Beer> getByName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Beer> query = session.createQuery(
                    "from Beer where name LIKE :name and isDeleted = false ", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByCountry(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Beer> query = session.createQuery(
                    "from Beer where country.name LIKE :name and isDeleted = false", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByStyle(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Beer> query = session.createQuery(
                    "from Beer where style.name LIKE :name and isDeleted = false", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByTags(String name) {
        List<Tag> tags = tagService.getByName(name);
        return tags.get(0).getBeerList();
    }

    @Override
    public List<Beer> sortByName() {
        try (Session session = sessionFactory.openSession()){
            Query<Beer> query = session.createQuery(
                    "from Beer where isDeleted = false order by name asc", Beer.class);
//            Query<Beer> query = session.createQuery("from Beer order by name desc", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortByAbv() {
        try (Session session = sessionFactory.openSession()){

            Query<Beer> query = session.createQuery("from Beer order by abv desc", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortByRating() {
        try (Session session = sessionFactory.openSession()){

            Query<Beer> query = session.createQuery("from Beer order by ratings desc", Beer.class);
            return query.list();
        }
    }


}
