package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.repositories.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(
                    "from Brewery where isDeleted = false", Brewery.class).list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null || brewery.isDeleted()) {
                throw new EntityNotFoundException(
                        String.format("Brewery with id %d not found", id));
            }
            return brewery;
        }
    }

    @Override
    public boolean checkBreweryExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public void update(Brewery brewery) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Brewery> getByName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Brewery> query = session.createQuery(
                    "from Brewery where name LIKE :name and isDeleted = false", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }
}
