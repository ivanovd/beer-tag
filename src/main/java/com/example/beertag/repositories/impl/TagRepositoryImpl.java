package com.example.beertag.repositories.impl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()){
            session.save(tag);
        }
    }

    @Override
    public List<Tag> getAll() {
        try(Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(
                            "from Tag where isDeleted = false", Tag.class)
                    .list();
        }
    }

    @Override
    public Tag getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return getById(id,session);
        }
    }

    @Override
    public boolean checkTagExists(String name) {
       return getByName(name).size() !=0;
    }

    @Override
    public void update(Tag tag) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery(
                    "from Tag where name LIKE :name and isDeleted = false", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();

        }
    }

    private Tag getById(int id, Session session) {
        Tag tag = session.get(Tag.class, id);
        if (tag == null || tag.isDeleted()) {
            throw new EntityNotFoundException(
                    String.format("Tag with id %d does not exist.", id)
            );
        }
        return tag;
    }

    @Override
    public List<Beer> getTagBeers(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = getById(tagId,session);
            return new ArrayList<>(tag.getBeerList());
        }
    }
}
