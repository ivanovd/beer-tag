package com.example.beertag.repositories;

import com.example.beertag.models.Beer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BeerRepository{

    void create(Beer beer);

    void update(Beer beer);

    List<Beer> getAll();

    boolean checkBeerExists(String name);

    Beer getById(int id);

    List<Beer> getByName(String name);

    List<Beer> filterByCountry (String name);

    List<Beer> filterByStyle (String name);

    List<Beer> filterByTags (String name);

    List<Beer> sortByName();

    List<Beer> sortByAbv();

    List<Beer> sortByRating();

}
