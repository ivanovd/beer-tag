package com.example.beertag.repositories;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;

import java.util.List;

public interface StyleRepository {

    void create(Style style);

    void update(Style style);

    boolean checkStyleExists(String name);

    List<Style> getAll();

    List<Style> getByName(String name);

    Style getById(int id);

    List<Beer> getStyleBeers(int styleId);

}
