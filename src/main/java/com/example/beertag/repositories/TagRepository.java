package com.example.beertag.repositories;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;

import java.util.List;

public interface TagRepository {

    void create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    boolean checkTagExists(String name);

    void update(Tag tag);

    List<Tag> getByName(String name);

    List<Beer> getTagBeers(int tagId);

}
