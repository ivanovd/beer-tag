package com.example.beertag.repositories;

import com.example.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {

    void create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    boolean checkBreweryExists(String name);

    void update(Brewery brewery);

    List<Brewery> getByName(String name);

}
