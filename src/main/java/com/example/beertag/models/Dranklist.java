package com.example.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "dranklist")
public class Dranklist {

    @Id
    @Column(name = "dranklist_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @PositiveOrZero(message = "Id should be positive or zero")
    private int wishlistId;

    @Column(name = "beer_id", insertable = false, updatable = false)
    private int beerId;

    @Column(name = "user_id", insertable = false, updatable = false)
    private int userId;

    public Dranklist() {

    }

    public int getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(int wishlistId) {
        this.wishlistId = wishlistId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
