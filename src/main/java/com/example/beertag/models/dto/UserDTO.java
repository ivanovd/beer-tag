package com.example.beertag.models.dto;

import javax.persistence.Column;
import javax.validation.constraints.Size;

public class UserDTO {

    @Size(min = 5, max = 20, message = "Username must be between 5 and 20")
    private String username;

    @Size(min = 2, max = 15, message = "First name should be between 2 and 15")
    private String firstName;

    @Size(min = 4, max = 20, message = "Last name should be between 4 and 20")
    private String lastName;

    @Column(name = "picture")
    private String picture;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    private String passwordConfirmation;

    public UserDTO() {

    }

    public UserDTO(@Size(min = 5, max = 20, message = "Username must be between 5 and 20") String username,
                   @Size(min = 2, max = 15, message = "First name should be between 2 and 15") String firstName,
                   @Size(min = 4, max = 20, message = "Last name should be between 4 and 20") String lastName,
                   String password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
