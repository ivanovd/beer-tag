package com.example.beertag.models.dto;

import javax.persistence.Column;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class BeerDTO {
    @Size(min = 2, max = 25, message = "Name size should be between 2 and 25 symbols")
    private String name;

    @Positive
    private double abv;

    @PositiveOrZero(message = "countryId should be positive or zero")
    private int countryId;

    @PositiveOrZero(message = "breweryId should be positive or zero")
    private int breweryId;

    @PositiveOrZero(message = "styleId should be positive or zero")
    private int styleId;

    @PositiveOrZero(message = "userId should be positive or zero")
    private int userId;

    private String description;

    @Column(name = "picture")
    private String picture;

    public BeerDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
