package com.example.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "beers_tags")
public class BeerTag {

    @Id
    @Column(name = "beer_tag_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @PositiveOrZero(message = "Id should be positive or zero")
    private int beerTagId;

    @Column(name = "beer_id", insertable = false, updatable = false)
    private int beerId;

    @Column(name = "tag_id", insertable = false, updatable = false)
    private int tagId;


    public BeerTag() {
    }

    public int getBeerTagId() {
        return beerTagId;
    }

    public void setBeerTagId(int beerTagId) {
        this.beerTagId = beerTagId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }
}
