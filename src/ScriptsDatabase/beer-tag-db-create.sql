-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for beer-tag-db
CREATE DATABASE IF NOT EXISTS `beer-tag-db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `beer-tag-db`;

-- Dumping structure for table beer-tag-db.authorities
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(10) NOT NULL,
  KEY `Authorities_Users` (`username`),
  CONSTRAINT `Authorities_Users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.beers
CREATE TABLE IF NOT EXISTS `beers` (
  `beer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `description` text DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `brewery_id` int(11) NOT NULL,
  `abv` double NOT NULL,
  `rating` double NOT NULL DEFAULT 0,
  `picture` blob DEFAULT NULL,
  `style_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`beer_id`),
  UNIQUE KEY `name` (`name`),
  KEY `Beers_Breweries` (`brewery_id`),
  KEY `Beers_Countries` (`country_id`),
  KEY `Beers_Styles` (`style_id`),
  KEY `Beers_Users` (`user_id`),
  CONSTRAINT `Beers_Breweries` FOREIGN KEY (`brewery_id`) REFERENCES `breweries` (`brewery_id`),
  CONSTRAINT `Beers_Countries` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`),
  CONSTRAINT `Beers_Styles` FOREIGN KEY (`style_id`) REFERENCES `styles` (`style_id`),
  CONSTRAINT `Beers_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.beers_tags
CREATE TABLE IF NOT EXISTS `beers_tags` (
  `beer_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`beer_tag_id`),
  KEY `Beers_Tags_Beers` (`beer_id`),
  KEY `Beers_Tags_Tags` (`tag_id`),
  CONSTRAINT `Beers_Tags_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Beers_Tags_Tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.breweries
CREATE TABLE IF NOT EXISTS `breweries` (
  `brewery_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`brewery_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.dranklist
CREATE TABLE IF NOT EXISTS `dranklist` (
  `dranklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`dranklist_id`),
  KEY `Dranklist_Beers` (`beer_id`),
  KEY `Dranklist_Users` (`user_id`),
  CONSTRAINT `Dranklist_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Dranklist_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.ratings
CREATE TABLE IF NOT EXISTS `ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` double NOT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `Ratings_Beers` (`beer_id`),
  KEY `Ratings_Users` (`user_id`),
  CONSTRAINT `Ratings_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Ratings_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.styles
CREATE TABLE IF NOT EXISTS `styles` (
  `style_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`style_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(68) NOT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `picture` blob DEFAULT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table beer-tag-db.wishlist
CREATE TABLE IF NOT EXISTS `wishlist` (
  `wishlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`wishlist_id`),
  KEY `Wishlist_Beers` (`beer_id`),
  KEY `Wishlist_Users` (`user_id`),
  CONSTRAINT `Wishlist_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Wishlist_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
