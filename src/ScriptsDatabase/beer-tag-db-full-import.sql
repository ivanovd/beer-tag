-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for beer-tag-db
CREATE DATABASE IF NOT EXISTS `beer-tag-db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `beer-tag-db`;

-- Dumping structure for table beer-tag-db.authorities
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(10) NOT NULL,
  KEY `Authorities_Users` (`username`),
  CONSTRAINT `Authorities_Users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.authorities: ~0 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.beers
CREATE TABLE IF NOT EXISTS `beers` (
  `beer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `description` text DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `brewery_id` int(11) NOT NULL,
  `abv` double NOT NULL,
  `rating` double NOT NULL DEFAULT 0,
  `picture` blob DEFAULT NULL,
  `style_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`beer_id`),
  UNIQUE KEY `name` (`name`),
  KEY `Beers_Breweries` (`brewery_id`),
  KEY `Beers_Countries` (`country_id`),
  KEY `Beers_Styles` (`style_id`),
  KEY `Beers_Users` (`user_id`),
  CONSTRAINT `Beers_Breweries` FOREIGN KEY (`brewery_id`) REFERENCES `breweries` (`brewery_id`),
  CONSTRAINT `Beers_Countries` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`),
  CONSTRAINT `Beers_Styles` FOREIGN KEY (`style_id`) REFERENCES `styles` (`style_id`),
  CONSTRAINT `Beers_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.beers: ~20 rows (approximately)
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
INSERT INTO `beers` (`beer_id`, `name`, `description`, `country_id`, `brewery_id`, `abv`, `rating`, `picture`, `style_id`, `user_id`, `is_deleted`) VALUES
	(1, 'Hydroxychlor', NULL, 65, 17, 32.04, 0, NULL, 7, 1, b'0'),
	(2, 'Nighttime', NULL, 127, 35, 24.77, 0, NULL, 6, 8, b'0'),
	(3, 'FUNGUS FREE', NULL, 20, 35, 33.26, 0, NULL, 4, 2, b'0'),
	(4, 'Spongia Tosta', NULL, 196, 3, 9, 0, NULL, 10, 6, b'0'),
	(5, 'CLE DE PEAU', NULL, 184, 34, 33.52, 0, NULL, 2, 6, b'0'),
	(6, 'Silace', NULL, 74, 24, 18.62, 0, NULL, 8, 4, b'0'),
	(7, 'Broad Spectrum ', NULL, 11, 24, 49.96, 0, NULL, 9, 8, b'0'),
	(8, 'Dexamethasone', NULL, 162, 15, 4.74, 0, NULL, 9, 8, b'0'),
	(9, 'Dextrose', NULL, 7, 29, 33.03, 0, NULL, 9, 18, b'0'),
	(10, 'Protonix', NULL, 63, 25, 22.59, 0, NULL, 1, 7, b'0'),
	(11, 'Furosemide', NULL, 23, 36, 4.37, 0, NULL, 9, 8, b'0'),
	(12, 'M-END', NULL, 185, 36, 34.64, 0, NULL, 3, 1, b'0'),
	(13, 'eB5 Age Sport', NULL, 192, 13, 31.91, 0, NULL, 5, 4, b'0'),
	(14, 'DIGOX', NULL, 104, 21, 17.76, 0, NULL, 8, 10, b'0'),
	(15, 'Moisture Renew', NULL, 54, 22, 37.55, 0, NULL, 4, 1, b'0'),
	(16, 'Fluoxetine', NULL, 116, 3, 37.49, 0, NULL, 3, 2, b'0'),
	(17, 'Muscle Maximizer', NULL, 215, 17, 12.56, 0, NULL, 6, 4, b'0'),
	(18, 'CIMETIDINE', NULL, 35, 46, 15.51, 0, NULL, 8, 4, b'0'),
	(19, 'Cipro', NULL, 168, 1, 48.44, 0, NULL, 3, 2, b'0'),
	(20, 'fluvoxamine ', NULL, 155, 19, 13.51, 0, NULL, 9, 9, b'0');
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.beers_tags
CREATE TABLE IF NOT EXISTS `beers_tags` (
  `beer_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`beer_tag_id`),
  KEY `Beers_Tags_Beers` (`beer_id`),
  KEY `Beers_Tags_Tags` (`tag_id`),
  CONSTRAINT `Beers_Tags_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Beers_Tags_Tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.beers_tags: ~0 rows (approximately)
/*!40000 ALTER TABLE `beers_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `beers_tags` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.breweries
CREATE TABLE IF NOT EXISTS `breweries` (
  `brewery_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`brewery_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.breweries: ~88 rows (approximately)
/*!40000 ALTER TABLE `breweries` DISABLE KEYS */;
INSERT INTO `breweries` (`brewery_id`, `name`, `is_deleted`) VALUES
	(1, 'Tazzy', b'0'),
	(2, 'Rhyzio', b'0'),
	(3, 'Rhynoodle', b'0'),
	(4, 'Lazz', b'0'),
	(5, 'Thoughtstorm', b'0'),
	(6, 'Izio', b'0'),
	(7, 'Centidel', b'0'),
	(8, 'Skiptube', b'0'),
	(9, 'Yozio', b'0'),
	(10, 'Dabvine', b'0'),
	(11, 'Mybuzz', b'0'),
	(12, 'Twitterlist', b'0'),
	(13, 'Meeveo', b'0'),
	(14, 'Blogspan', b'0'),
	(15, 'Thoughtmix', b'0'),
	(16, 'Tagchat', b'0'),
	(17, 'Kamba', b'0'),
	(18, 'Devify', b'0'),
	(19, 'Geba', b'0'),
	(20, 'Oyope', b'0'),
	(21, 'Skajo', b'0'),
	(22, 'Jabbersphere', b'0'),
	(23, 'Zoomcast', b'0'),
	(24, 'Feedbug', b'0'),
	(25, 'Feedfish', b'0'),
	(26, 'Skimia', b'0'),
	(27, 'Edgeify', b'0'),
	(28, 'Tagcat', b'0'),
	(29, 'Livetube', b'0'),
	(30, 'Zoonder', b'0'),
	(31, 'Trudeo', b'0'),
	(32, 'Zooxo', b'0'),
	(33, 'Shufflebeat', b'0'),
	(34, 'Zoonoodle', b'0'),
	(35, 'Cogidoo', b'0'),
	(36, 'Quaxo', b'0'),
	(37, 'Omba', b'0'),
	(38, 'Skipfire', b'0'),
	(39, 'Linkbridge', b'0'),
	(40, 'Eayo', b'0'),
	(41, 'Zoomdog', b'0'),
	(42, 'Vinder', b'0'),
	(43, 'Brightdog', b'0'),
	(44, 'Cogibox', b'0'),
	(45, 'Rhyloo', b'0'),
	(46, 'Riffpath', b'0'),
	(47, 'Avaveo', b'0'),
	(48, 'Oodoo', b'0'),
	(49, 'Babbleset', b'0'),
	(50, 'Topicstorm', b'0'),
	(51, 'Yakitri', b'0'),
	(52, 'Twimm', b'0'),
	(53, 'Gabtune', b'0'),
	(54, 'Trunyx', b'0'),
	(55, 'LiveZ', b'0'),
	(56, 'Tagopia', b'0'),
	(57, 'Oyoloo', b'0'),
	(58, 'Yacero', b'0'),
	(59, 'Flashpoint', b'0'),
	(60, 'Devcast', b'0'),
	(61, 'Wordtune', b'0'),
	(62, 'Fivebridge', b'0'),
	(63, 'Jabberbean', b'0'),
	(64, 'Skynoodle', b'0'),
	(65, 'Wordify', b'0'),
	(66, 'Edgetag', b'0'),
	(67, 'Browsezoom', b'0'),
	(68, 'Kaymbo', b'0'),
	(69, 'Youfeed', b'0'),
	(70, 'Trudoo', b'0'),
	(71, 'Skilith', b'0'),
	(72, 'Twinte', b'0'),
	(73, 'Innojam', b'0'),
	(74, 'Eimbee', b'0'),
	(75, 'Skinix', b'0'),
	(76, 'Dabfeed', b'0'),
	(77, 'Ainyx', b'0'),
	(78, 'Realbridge', b'0'),
	(79, 'DabZ', b'0'),
	(80, 'Pixoboo', b'0'),
	(81, 'Wordware', b'0'),
	(82, 'Youspan', b'0'),
	(83, 'Quamba', b'0'),
	(84, 'Flipopia', b'0'),
	(85, 'Meejo', b'0'),
	(86, 'Oozz', b'0'),
	(87, 'Riffwire', b'0'),
	(88, 'Buzzbean', b'0');
/*!40000 ALTER TABLE `breweries` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.countries: ~250 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`country_id`, `name`, `is_deleted`) VALUES
	(1, 'Afghanistan', b'0'),
	(2, 'Åland', b'0'),
	(3, 'Albania', b'0'),
	(4, 'Algeria', b'0'),
	(5, 'American Samoa', b'0'),
	(6, 'Andorra', b'0'),
	(7, 'Angola', b'0'),
	(8, 'Anguilla', b'0'),
	(9, 'Antarctica', b'0'),
	(10, 'Antigua and Barbuda', b'0'),
	(11, 'Argentina', b'0'),
	(12, 'Armenia', b'0'),
	(13, 'Aruba', b'0'),
	(14, 'Australia', b'0'),
	(15, 'Austria', b'0'),
	(16, 'Azerbaijan', b'0'),
	(17, 'Bahamas', b'0'),
	(18, 'Bahrain', b'0'),
	(19, 'Bangladesh', b'0'),
	(20, 'Barbados', b'0'),
	(21, 'Belarus', b'0'),
	(22, 'Belgium', b'0'),
	(23, 'Belize', b'0'),
	(24, 'Benin', b'0'),
	(25, 'Bermuda', b'0'),
	(26, 'Bhutan', b'0'),
	(27, 'Bolivia', b'0'),
	(28, 'Bonaire', b'0'),
	(29, 'Bosnia and Herzegovina', b'0'),
	(30, 'Botswana', b'0'),
	(31, 'Bouvet Island', b'0'),
	(32, 'Brazil', b'0'),
	(33, 'British Indian Ocean Territory', b'0'),
	(34, 'British Virgin Islands', b'0'),
	(35, 'Brunei', b'0'),
	(36, 'Bulgaria', b'0'),
	(37, 'Burkina Faso', b'0'),
	(38, 'Burundi', b'0'),
	(39, 'Cambodia', b'0'),
	(40, 'Cameroon', b'0'),
	(41, 'Canada', b'0'),
	(42, 'Cape Verde', b'0'),
	(43, 'Cayman Islands', b'0'),
	(44, 'Central African Republic', b'0'),
	(45, 'Chad', b'0'),
	(46, 'Chile', b'0'),
	(47, 'China', b'0'),
	(48, 'Christmas Island', b'0'),
	(49, 'Cocos [Keeling] Islands', b'0'),
	(50, 'Colombia', b'0'),
	(51, 'Comoros', b'0'),
	(52, 'Cook Islands', b'0'),
	(53, 'Costa Rica', b'0'),
	(54, 'Croatia', b'0'),
	(55, 'Cuba', b'0'),
	(56, 'Curacao', b'0'),
	(57, 'Cyprus', b'0'),
	(58, 'Czech Republic', b'0'),
	(59, 'Democratic Republic of the Congo', b'0'),
	(60, 'Denmark', b'0'),
	(61, 'Djibouti', b'0'),
	(62, 'Dominica', b'0'),
	(63, 'Dominican Republic', b'0'),
	(64, 'East Timor', b'0'),
	(65, 'Ecuador', b'0'),
	(66, 'Egypt', b'0'),
	(67, 'El Salvador', b'0'),
	(68, 'Equatorial Guinea', b'0'),
	(69, 'Eritrea', b'0'),
	(70, 'Estonia', b'0'),
	(71, 'Ethiopia', b'0'),
	(72, 'Falkland Islands', b'0'),
	(73, 'Faroe Islands', b'0'),
	(74, 'Fiji', b'0'),
	(75, 'Finland', b'0'),
	(76, 'France', b'0'),
	(77, 'French Guiana', b'0'),
	(78, 'French Polynesia', b'0'),
	(79, 'French Southern Territories', b'0'),
	(80, 'Gabon', b'0'),
	(81, 'Gambia', b'0'),
	(82, 'Georgia', b'0'),
	(83, 'Germany', b'0'),
	(84, 'Ghana', b'0'),
	(85, 'Gibraltar', b'0'),
	(86, 'Greece', b'0'),
	(87, 'Greenland', b'0'),
	(88, 'Grenada', b'0'),
	(89, 'Guadeloupe', b'0'),
	(90, 'Guam', b'0'),
	(91, 'Guatemala', b'0'),
	(92, 'Guernsey', b'0'),
	(93, 'Guinea', b'0'),
	(94, 'Guinea-Bissau', b'0'),
	(95, 'Guyana', b'0'),
	(96, 'Haiti', b'0'),
	(97, 'Heard Island and McDonald Islands', b'0'),
	(98, 'Honduras', b'0'),
	(99, 'Hong Kong', b'0'),
	(100, 'Hungary', b'0'),
	(101, 'Iceland', b'0'),
	(102, 'India', b'0'),
	(103, 'Indonesia', b'0'),
	(104, 'Iran', b'0'),
	(105, 'Iraq', b'0'),
	(106, 'Ireland', b'0'),
	(107, 'Isle of Man', b'0'),
	(108, 'Israel', b'0'),
	(109, 'Italy', b'0'),
	(110, 'Ivory Coast', b'0'),
	(111, 'Jamaica', b'0'),
	(112, 'Japan', b'0'),
	(113, 'Jersey', b'0'),
	(114, 'Jordan', b'0'),
	(115, 'Kazakhstan', b'0'),
	(116, 'Kenya', b'0'),
	(117, 'Kiribati', b'0'),
	(118, 'Kosovo', b'0'),
	(119, 'Kuwait', b'0'),
	(120, 'Kyrgyzstan', b'0'),
	(121, 'Laos', b'0'),
	(122, 'Latvia', b'0'),
	(123, 'Lebanon', b'0'),
	(124, 'Lesotho', b'0'),
	(125, 'Liberia', b'0'),
	(126, 'Libya', b'0'),
	(127, 'Liechtenstein', b'0'),
	(128, 'Lithuania', b'0'),
	(129, 'Luxembourg', b'0'),
	(130, 'Macao', b'0'),
	(131, 'Macedonia', b'0'),
	(132, 'Madagascar', b'0'),
	(133, 'Malawi', b'0'),
	(134, 'Malaysia', b'0'),
	(135, 'Maldives', b'0'),
	(136, 'Mali', b'0'),
	(137, 'Malta', b'0'),
	(138, 'Marshall Islands', b'0'),
	(139, 'Martinique', b'0'),
	(140, 'Mauritania', b'0'),
	(141, 'Mauritius', b'0'),
	(142, 'Mayotte', b'0'),
	(143, 'Mexico', b'0'),
	(144, 'Micronesia', b'0'),
	(145, 'Moldova', b'0'),
	(146, 'Monaco', b'0'),
	(147, 'Mongolia', b'0'),
	(148, 'Montenegro', b'0'),
	(149, 'Montserrat', b'0'),
	(150, 'Morocco', b'0'),
	(151, 'Mozambique', b'0'),
	(152, 'Myanmar [Burma]', b'0'),
	(153, 'Namibia', b'0'),
	(154, 'Nauru', b'0'),
	(155, 'Nepal', b'0'),
	(156, 'Netherlands', b'0'),
	(157, 'New Caledonia', b'0'),
	(158, 'New Zealand', b'0'),
	(159, 'Nicaragua', b'0'),
	(160, 'Niger', b'0'),
	(161, 'Nigeria', b'0'),
	(162, 'Niue', b'0'),
	(163, 'Norfolk Island', b'0'),
	(164, 'North Korea', b'0'),
	(165, 'Northern Mariana Islands', b'0'),
	(166, 'Norway', b'0'),
	(167, 'Oman', b'0'),
	(168, 'Pakistan', b'0'),
	(169, 'Palau', b'0'),
	(170, 'Palestine', b'0'),
	(171, 'Panama', b'0'),
	(172, 'Papua New Guinea', b'0'),
	(173, 'Paraguay', b'0'),
	(174, 'Peru', b'0'),
	(175, 'Philippines', b'0'),
	(176, 'Pitcairn Islands', b'0'),
	(177, 'Poland', b'0'),
	(178, 'Portugal', b'0'),
	(179, 'Puerto Rico', b'0'),
	(180, 'Qatar', b'0'),
	(181, 'Republic of the Congo', b'0'),
	(182, 'Réunion', b'0'),
	(183, 'Romania', b'0'),
	(184, 'Russia', b'0'),
	(185, 'Rwanda', b'0'),
	(186, 'Saint Barthélemy', b'0'),
	(187, 'Saint Helena', b'0'),
	(188, 'Saint Kitts and Nevis', b'0'),
	(189, 'Saint Lucia', b'0'),
	(190, 'Saint Martin', b'0'),
	(191, 'Saint Pierre and Miquelon', b'0'),
	(192, 'Saint Vincent and the Grenadines', b'0'),
	(193, 'Samoa', b'0'),
	(194, 'San Marino', b'0'),
	(195, 'São Tomé and Príncipe', b'0'),
	(196, 'Saudi Arabia', b'0'),
	(197, 'Senegal', b'0'),
	(198, 'Serbia', b'0'),
	(199, 'Seychelles', b'0'),
	(200, 'Sierra Leone', b'0'),
	(201, 'Singapore', b'0'),
	(202, 'Sint Maarten', b'0'),
	(203, 'Slovakia', b'0'),
	(204, 'Slovenia', b'0'),
	(205, 'Solomon Islands', b'0'),
	(206, 'Somalia', b'0'),
	(207, 'South Africa', b'0'),
	(208, 'South Georgia and the South Sandwich Islands', b'0'),
	(209, 'South Korea', b'0'),
	(210, 'South Sudan', b'0'),
	(211, 'Spain', b'0'),
	(212, 'Sri Lanka', b'0'),
	(213, 'Sudan', b'0'),
	(214, 'Suriname', b'0'),
	(215, 'Svalbard and Jan Mayen', b'0'),
	(216, 'Swaziland', b'0'),
	(217, 'Sweden', b'0'),
	(218, 'Switzerland', b'0'),
	(219, 'Syria', b'0'),
	(220, 'Taiwan', b'0'),
	(221, 'Tajikistan', b'0'),
	(222, 'Tanzania', b'0'),
	(223, 'Thailand', b'0'),
	(224, 'Togo', b'0'),
	(225, 'Tokelau', b'0'),
	(226, 'Tonga', b'0'),
	(227, 'Trinidad and Tobago', b'0'),
	(228, 'Tunisia', b'0'),
	(229, 'Turkey', b'0'),
	(230, 'Turkmenistan', b'0'),
	(231, 'Turks and Caicos Islands', b'0'),
	(232, 'Tuvalu', b'0'),
	(233, 'U.S. Minor Outlying Islands', b'0'),
	(234, 'U.S. Virgin Islands', b'0'),
	(235, 'Uganda', b'0'),
	(236, 'Ukraine', b'0'),
	(237, 'United Arab Emirates', b'0'),
	(238, 'United Kingdom', b'0'),
	(239, 'United States', b'0'),
	(240, 'Uruguay', b'0'),
	(241, 'Uzbekistan', b'0'),
	(242, 'Vanuatu', b'0'),
	(243, 'Vatican City', b'0'),
	(244, 'Venezuela', b'0'),
	(245, 'Vietnam', b'0'),
	(246, 'Wallis and Futuna', b'0'),
	(247, 'Western Sahara', b'0'),
	(248, 'Yemen', b'0'),
	(249, 'Zambia', b'0'),
	(250, 'Zimbabwe', b'0');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.dranklist
CREATE TABLE IF NOT EXISTS `dranklist` (
  `dranklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`dranklist_id`),
  KEY `Dranklist_Beers` (`beer_id`),
  KEY `Dranklist_Users` (`user_id`),
  CONSTRAINT `Dranklist_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Dranklist_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.dranklist: ~0 rows (approximately)
/*!40000 ALTER TABLE `dranklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `dranklist` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.ratings
CREATE TABLE IF NOT EXISTS `ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` double NOT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `Ratings_Beers` (`beer_id`),
  KEY `Ratings_Users` (`user_id`),
  CONSTRAINT `Ratings_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Ratings_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.ratings: ~0 rows (approximately)
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.styles
CREATE TABLE IF NOT EXISTS `styles` (
  `style_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`style_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.styles: ~20 rows (approximately)
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` (`style_id`, `name`, `is_deleted`) VALUES
	(1, 'Altbier', b'0'),
	(2, 'Amber ale', b'0'),
	(3, 'Barley wine', b'0'),
	(4, 'Berliner Weisse', b'0'),
	(5, 'Bière de Garde', b'0'),
	(6, 'Bitter', b'0'),
	(7, 'Blonde Ale', b'0'),
	(8, 'Bock', b'0'),
	(9, 'Brown ale', b'0'),
	(10, 'California Common/Steam Beer', b'0'),
	(11, 'Cream Ale', b'0'),
	(12, 'Dortmunder Export', b'0'),
	(13, 'Doppelbock', b'0'),
	(14, 'Dunkel', b'0'),
	(15, 'Dunkelweizen', b'0'),
	(16, 'Eisbock', b'0'),
	(17, 'Flanders red ale', b'0'),
	(18, 'Golden/Summer ale', b'0'),
	(19, 'Gose', b'0'),
	(20, 'Gueuze', b'0'),
    (21, 'Hefeweizen', b'0'),
    (22, 'Helles', b'0'),
    (23, 'India pale ale', b'0'),
    (24, 'Kölsch', b'0'),
    (25, 'Lambic', b'0'),
    (26, 'Light', b'0'),
    (27, 'Light Ale', b'0'),
    (28, 'Maibock/Helles bock', b'0'),
    (29, 'Malt liquor', b'0'),
    (30, 'Mild', b'0'),
    (31, 'Oktoberfestbier/Märzenbier', b'0'),
    (32, 'Old ale', b'0'),
    (33, 'Oud bruin', b'0'),
    (34, 'Pale ale', b'0'),
    (35, 'Pilsener/Pilsner/Pils', b'0'),
    (36, 'Porter', b'0'),
    (37, 'Red ale', b'0'),
    (38, 'Roggenbier', b'0'),
    (39, 'Saison', b'0'),
    (40, 'Scotch ale', b'0'),
    (41, 'Stout', b'0'),
    (42, 'Schwarzbier', b'0'),
    (43, 'Vienna lager', b'0'),
    (44, 'Witbier', b'0'),
    (45, 'Weissbier', b'0'),
    (46, 'Weizenbock', b'0');
    /*!40000 ALTER TABLE `styles` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.tags: ~10 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `name`, `is_deleted`) VALUES
	(1, 'optimal', b'0'),
	(2, '24/7', b'0'),
	(3, 'intangible', b'0'),
	(4, 'zero administration', b'0'),
	(5, 'toolset', b'0'),
	(6, 'data-warehouse', b'0'),
	(7, 'real-time', b'0'),
	(8, 'human-resource', b'0'),
	(9, 'Advanced', b'0'),
	(10, 'Fully-configurable', b'0');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(68) NOT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `picture` blob DEFAULT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.users: ~20 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `first_name`, `last_name`, `email`, `picture`, `enabled`) VALUES
	(1, 'cclail0', 'dNRP8qK', 'Corny', 'Clail', 'cclail0@stanford.edu', NULL, b'1'),
	(2, 'pwestfalen1', 'BjW1O37ay', 'Pat', 'Westfalen', 'pwestfalen1@bigcartel.com', NULL, b'1'),
	(3, 'ineasam2', 'iA6dPaR', 'Isac', 'Neasam', 'ineasam2@dedecms.com', NULL, b'1'),
	(4, 'cbridden3', 'I6FGAup7b', 'Cathie', 'Bridden', 'cbridden3@youtube.com', NULL, b'1'),
	(5, 'ppykett4', '4C3pA9wq6K', 'Paco', 'Pykett', 'ppykett4@wix.com', NULL, b'1'),
	(6, 'emacconnell5', 'HvjgAosEGQy', 'Eward', 'MacConnell', 'emacconnell5@accuweather.com', NULL, b'1'),
	(7, 'hhauger6', 'vmNMZzDx5xYf', 'Hatti', 'Hauger', 'hhauger6@businessweek.com', NULL, b'1'),
	(8, 'vcudihy7', '6mQfu6T', 'Vyky', 'Cudihy', 'vcudihy7@thetimes.co.uk', NULL, b'1'),
	(9, 'eborges8', 'QMbIblzoV', 'Ellsworth', 'Borges', 'eborges8@imdb.com', NULL, b'1'),
	(10, 'ftibb9', 'OZtJ7Ty1rPB2', 'Farrand', 'Tibb', 'ftibb9@tripadvisor.com', NULL, b'1'),
	(11, 'pcaplana', 'YcuMUtMmucw6', 'Priscella', 'Caplan', 'pcaplana@fastcompany.com', NULL, b'1'),
	(12, 'britchmanb', 'nHnxSL', 'Brynna', 'Ritchman', 'britchmanb@foxnews.com', NULL, b'1'),
	(13, 'kharnesc', 'cH1tUlMcuK', 'Keriann', 'Harnes', 'kharnesc@wikispaces.com', NULL, b'1'),
	(14, 'gcowapd', 'D4LMaz0Mb', 'Giorgio', 'Cowap', 'gcowapd@wsj.com', NULL, b'1'),
	(15, 'cboolere', 'RQauck', 'Camey', 'Booler', 'cboolere@comcast.net', NULL, b'1'),
	(16, 'rrawf', 'pIaEI5C', 'Rayner', 'Raw', 'rrawf@csmonitor.com', NULL, b'1'),
	(17, 'ghackleyg', 'jfoeKa', 'Giulio', 'Hackley', 'ghackleyg@joomla.org', NULL, b'1'),
	(18, 'ncuphush', 'UxEoLhdxS1N', 'Nealon', 'Cuphus', 'ncuphush@msu.edu', NULL, b'1'),
	(19, 'vblazici', 'Os3xmHZeHUZ7', 'Vally', 'Blazic', 'vblazici@fotki.com', NULL, b'1'),
	(20, 'rbonteinj', '0kdnT95euuh', 'Rourke', 'Bontein', 'rbonteinj@cbslocal.com', NULL, b'1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table beer-tag-db.wishlist
CREATE TABLE IF NOT EXISTS `wishlist` (
  `wishlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`wishlist_id`),
  KEY `Wishlist_Beers` (`beer_id`),
  KEY `Wishlist_Users` (`user_id`),
  CONSTRAINT `Wishlist_Beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  CONSTRAINT `Wishlist_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table beer-tag-db.wishlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
